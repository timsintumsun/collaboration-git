<h1 align="center">Welcome to Cineflix 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1.0-blue.svg?cacheSeconds=2592000" />
</p>

### 🏠 [Homepage](https://gitlab.com/binar-10-mini-project/team-f/team-f-frontend/movie-app)

### ✨ [Demo](https://cineflix-apps.herokuapp.com/)

## Install

```sh
yarn install
```

## Usage

```sh
yarn run start
```
> Cineflix is a mini project movie review website. Search, find and give reviews to your favorite movies, and read all reviews provided by other people.
=======
## Run tests

```sh
yarn run test
```

## Author

👤 **Group F**

## Show your support
